$('.faq').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('faq--active');
  $(this).find('.faq__answer').slideToggle('fast');
})
